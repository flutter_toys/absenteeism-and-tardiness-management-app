# ChoolCheck(出席チェックの略した言葉)

<details>
  <summary>the first screen of the app</summary>
<img src="https://gitlab.com/flutter_toys/absenteeism-and-tardiness-management-app/uploads/399e7c957960b31ee2a8b2c9b5365a97/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-01-10_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_4.52.46.png" height ="50%" width ="50%"></img>
</details>

## Getting Started
This application uses the following packages:
```
  google_maps_flutter: ^2.1.1
  geolocator: ^8.1.1
```
**For this project**, You have to stick to **three** points.

First of All, get the API key from the **Google API console**. 

Second, add api key values to Android and iOS folder. 

Lastly, please refer to the URL **below** to proceed with the correct setup of the package.
>[google_maps_flutter](https://pub.dev/packages/google_maps_flutter)

>[geolocator](https://pub.dev/packages/geolocator)


## Functional Specification

1. A **blue** circle is placed within a **100m** radius of the companie's location below, So you can only operate within that radius.
  ```
    static final LatLng companyLatLng = LatLng(
    37.5233273,
    126.921252,
  );
  
  ```
2. If you press the **출근하기** button within the radius, the circle and bottom icons turn green and are disabled.(To prevent collisions, errors, and multiple attempts)

3. If your location is not within that radius, the circle and button UI will turn **red** and **de**activated(cannot be pressed).

4. Touch the blue icon in the **upper right** corner of the screen to move the camera on the map to its current position.

<details>
  <summary>Application snapshot</summary>
  <img src="https://gitlab.com/flutter_toys/absenteeism-and-tardiness-management-app/uploads/399e7c957960b31ee2a8b2c9b5365a97/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-01-10_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_4.52.46.png" width = "50%"name="image-name">
  <img src="https://gitlab.com/flutter_toys/absenteeism-and-tardiness-management-app/uploads/6bcf450af6a8c6095fc96a2d30fb8f9d/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-01-10_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_4.57.04.png" width = "50%" name="image-name">
  <img src="https://gitlab.com/flutter_toys/absenteeism-and-tardiness-management-app/uploads/baed89daea5d0fc32608cb1d3d0afd37/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-01-10_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_4.57.11.png" width = "50%" name="image-name">
  <img src="https://gitlab.com/flutter_toys/absenteeism-and-tardiness-management-app/uploads/d39d88a386622fe39106c65a62b01a8d/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2023-01-10_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_4.56.04.png" width = "50%" name="image-name">
</details>

#### ※ Caution
The following error occurs in the current package version
<br>
only on debug mode with **ios emulator**
```
doesn't work camera position button on Functional Specifications No. 4
```



Nevertheless, it is recommended that you change the mode to **release mode** or the package version above to the latest version if you want to keep using **ios emulator**. 


